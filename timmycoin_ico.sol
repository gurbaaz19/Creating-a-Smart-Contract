// TimmyCoin ICO

// Compiler Version
pragma solidity ^0.8.0;

contract timmycoin_ico {
    // Introducing total number of TimmyCoins to be circulated
    uint public max_timmycoins = 1000000;

    // Introducing the USD - TimmyCoin conversion rate
    uint public used_to_timmycoins = 1000;

    // Introducing the Timmy Coins bought by investors

    uint public total_timmycoins_bought = 0;
}
